Initially, I wanted to use templates to construct various
pages within the website. This was set aside in favor of
hand-crafting the original pages in the site. I hope to
reintroduce templates and some type of static site
generation at some point in the future.