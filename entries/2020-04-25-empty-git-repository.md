```sh

git init
# Initialized empty Git repository in /tmp/git-empty-repo/.git/

# Purposeful failure
git commit --message="Initial Commit"
# On branch master
#
# Initial commit
#
# nothing to commit

git commit --allow-empty --message="Initial Commit"
# [master (root-commit) 5e4e3ac] Initial Commit

git log
# commit 5e4e3ac63f22c7b872dc1494c18f5bab5c0fde97 (HEAD -> master)
# Author: John Long <john@128.io>
# Date:   Sat Apr 25 11:56:17 2020 -0500
#
#     Initial Commit

```