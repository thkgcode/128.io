# Dev Diary: Setting up Gitea/Drone/Minio --- 2020-04-26

Context:

  Due to a variety of reasons, I'd like to move away from
  GitLab for software development into self-hosted instances
  of Gitea, Drone, and Minio.

Initial Requirements / Thoughts:

- Run Gitea, Drone, and Minio through Docker
- Use Gitea for hosting repositories
- Use Drone for CI/CD
- Use Minio to store build assets from Drone

Build Log:

- I'm starting from scratch here, so my first task is to
  get instances of Gitea, Drone, and Minio running locally.
  Searching through the Docker Hub [1], I am able to find
  official images offered for each service.
- Gitea requires a database to operate. I've chosen MariaDB
  out of familiarity.
- To avoid remembering port numbers for each service, I've
  opted to use an Nginx image I maintain. It acts as a prox
  to route requests based off the subdomain to each service.
- In general, I like to hard-code specific tags to ensure
  nothing breaks unexpectedly if an image has
  backward-incompatible changes. Adding a comment linking
  to the tags page acts as a shortcut when identifying if
  the image is out of date and may be updated.
- For similar reasons, I try to set memory limits for all
  containers at fairly restrictive values. If containers
  stop running due to memory, or if block I/O appears to
  be too large for the operations performed, I'll increase
  if there's available memory.
- After attempting to get Drone integrated with Gitea, I
  realized using 127.0.0.1 for hostnames won't work. Using
  172.17.0.1 (the default gateway IP for Linux installations
  of Docker), I was able to fix and get the integration
  between Gitea and Drone functional.
- Minio in its current setup is standalone from Gitea and
  Drone. In an upcoming post, this will change once we
  start to work on CI/CD for my personal website.
- The docker-compose.yml file I ended up with:
  ```yml
  version: '2.4'
  services:

    proxy:
      image: adduc/nginx
      mem_limit: 32M
      stop_signal: SIGKILL
      ports: [ "172.17.0.1:80:80" ]
      volumes:
        - ./data/proxy:/var/log/nginx
      environment:
        # http://gitea.172.17.0.1.nip.io/
        HOST_GITEA: gitea proxy /srv/empty gitea 3000

        # http://drone.172.17.0.1.nip.io/
        HOST_DRONE: drone proxy /srv/empty drone 80

        # http://minio.172.17.0.1.nip.io/
        HOST_MINIO: minio proxy /srv/enmpty minio 9000

    gitea:
      # https://hub.docker.com/r/gitea/gitea/tags
      image: gitea/gitea:1.11.4
      mem_limit: 256M
      volumes:
        - ./data/gitea:/data
      ports: [ "172.17.0.1:2222:22" ]

    mariadb:
      # https://hub.docker.com/_/mariadb?tab=tags
      image: mariadb:10.4
      mem_limit: 256M
      volumes:
        - ./data/mariadb:/var/lib/mysql
      ports:
        - "172.17.0.1:3306:3306"
      environment:
        MYSQL_ROOT_PASSWORD: [[REDACTED]]
        MYSQL_DATABASE: gitea
        MYSQL_USER: gitea
        MYSQL_PASSWORD: [[REDACTED]]

    drone:
      # https://hub.docker.com/r/drone/drone/tags
      image: drone/drone:1.7.0
      mem_limit: 64M
      environment:
        DRONE_GITEA_CLIENT_ID: [[REDACTED]]
        DRONE_GITEA_CLIENT_SECRET: [[REDACTED]]
        DRONE_GITEA_SERVER: http://gitea.172.17.0.1.nip.io
        DRONE_RPC_SECRET: &DRONE_RPC_SECRET [[REDACTED]]
        DRONE_SERVER_HOST: &DRONE_SERVER_HOST drone.172.17.0.1.nip.io
        DRONE_SERVER_PROTO: &DRONE_SERVER_PROTO http

    runner:
      # https://hub.docker.com/r/drone/drone-runner-docker/tags
      image: drone/drone-runner-docker:1.3
      mem_limit: 64M
      environment:
        DRONE_RPC_HOST: *DRONE_SERVER_HOST
        DRONE_RPC_PROTO: *DRONE_SERVER_PROTO
        DRONE_RPC_SECRET: *DRONE_RPC_SECRET
        DRONE_RUNNER_CAPACITY: 2
        DRONE_RUNNER_NAME: runner
      volumes:
        - /var/run/docker.sock:/var/run/docker.sock

    minio:
      # https://hub.docker.com/r/minio/minio/tags
      image: minio/minio:RELEASE.2020-04-23T00-58-49Z
      mem_limit: 128M
      command: server /data
      volumes:
        - ./data/minio:/data
      environment:
        MINIO_ACCESS_KEY: gitea
        MINIO_SECRET_KEY: [[REDACTED]]
        MINIO_DOMAIN: minio.172.17.0.1.nip.io

  ```

Ideas for the future:

- Configure multiple users within Minio to restrict build
  artifact access. Less of a concern at the moment, but may
  be useful when Drone starts to be used for private
  repositories.
- Move to somewhere publicly accessible
- Add SSL certificates

Relevant links:

1. Docker Hub: https://hub.docker.com/
2. Drone Gitea Integration: https://docs.drone.io/server/provider/gitea/
3. Drone Runner Installation: https://docs.drone.io/runner/docker/installation/linux/